import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppState();
}

bool _attendance = false;
String _code = '';

class _AppState extends State<MyApp> {
  _turnOn() async {
    await FlutterBluetoothSerial.instance.requestDiscoverable(3600);
  }

  String getDate() {
    var now = DateTime.now();
    return '${now.day}-${now.month - 1}-${now.year}';
  }

  Future _makeGetRequest() => http.get(
        'https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/${getDate()}/students/$_code',
      );

  getAttendance(String string) {
    Map<String, Object> a = json.decode(string);
    print(a);
    return !a.containsKey("error");
  }

  _ackAlert(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          content: const Text(
              "You must type a code in order to check the student's attendance"),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Bluetooth app',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Bluetooth tool'),
            centerTitle: true,
          ),
          body: Builder(
            builder: (context) => Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100.0, vertical: 8.0),
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Enter your code',
                          hasFloatingPlaceholder: true),
                      onChanged: (e) => setState(() => _code = e),
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    height: 50,
                    child: RaisedButton(
                      color: Colors.blue,
                      textColor: Colors.white,
                      child: Text(
                        'Press to check for attendance today',
                        textAlign: TextAlign.center,
                      ),
                      onPressed: () => _code.length > 0
                          ? _makeGetRequest().then(
                              (val) => setState(() {
                                _attendance = getAttendance(val.body);
                              }),
                            )
                          : _ackAlert(context),
                    ),
                  ),
                  Checkbox(value: _attendance, onChanged: null),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => _turnOn(),
            child: Icon(Icons.bluetooth),
          ),
        ));
  }
}
